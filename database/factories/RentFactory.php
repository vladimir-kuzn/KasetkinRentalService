<?php

namespace Database\Factories;

use App\Models\Rent;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Rent>
 */
class RentFactory extends Factory
{
    protected $model = Rent::class;

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $start_date = $this->faker->dateTimeBetween('-1 month', 'now');
        $end_date = (clone $start_date)->modify('+'.rand(1, 30).' days');

        return [
            'employee_id' => \App\Models\Employee::factory(),
            'client_id' => \App\Models\Client::factory(),
            'inventory_id' => \App\Models\Inventory::factory(),
            'start_date' => $start_date->format('Y-m-d'),
            'end_date' => $end_date->format('Y-m-d'),
            'total_cost' => $this->faker->randomFloat(2, 100, 1000),
            'status' => $this->faker->randomElement(['active', 'closed']),
        ];
    }
}
