@extends('layouts.app')

@section('content')
    <div class="container mx-auto px-4">
        <h1 class="text-2xl font-bold mb-4">Редактирование клиента</h1>
        @include('clients.form', ['action' => route('clients.update', $client->id), 'client' => $client])
    </div>
@endsection
