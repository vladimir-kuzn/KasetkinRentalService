<form action="{{ $action }}" method="POST" class="w-full max-w-lg">
    @csrf
    @if(isset($client)) @method('PUT') @endif

    <div class="mb-4">
        <label for="name" class="block text-gray-700 text-sm font-bold mb-2">Имя:</label>
        <input type="text" name="name" id="name" value="{{ $client->name ?? '' }}" required class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline">
    </div>

    <div class="mb-4">
        <label for="email" class="block text-gray-700 text-sm font-bold mb-2">Email:</label>
        <input type="email" name="email" id="email" value="{{ $client->email ?? '' }}" required class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline">
    </div>

    <div class="mb-4">
        <label for="phone_number" class="block text-gray-700 text-sm font-bold mb-2">Телефон:</label>
        <input type="text" name="phone_number" id="phone_number" value="{{ $client->phone_number ?? '' }}" required class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline">
    </div>

    <div class="flex items-center justify-between">
        <button type="submit" class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline">Сохранить</button>
    </div>
</form>
