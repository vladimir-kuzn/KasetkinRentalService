@if(isset($rent))
    <form action="{{ route('rents.update', $rent->id) }}" method="POST" class="w-full max-w-lg">
        @method('PUT')
        @else
            <form action="{{ route('rents.store') }}" method="POST" class="w-full max-w-lg">
                @endif
                @csrf

                <div class="mb-4">
                    <label for="employee_id" class="block text-gray-700 text-sm font-bold mb-2">Сотрудник:</label>
                    <select name="employee_id" id="employee_id" required
                            class="shadow border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline">
                        @foreach($employees as $employee)
                            <option
                                value="{{ $employee->id }}" {{ (isset($rent) && $rent->employee_id == $employee->id) ? 'selected' : '' }}>
                                {{ $employee->name }}
                            </option>
                        @endforeach
                    </select>
                </div>

                <div class="mb-4">
                    <label for="client_id" class="block text-gray-700 text-sm font-bold mb-2">Клиент:</label>
                    <select name="client_id" id="client_id" required
                            class="shadow border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline">
                        @foreach($clients as $client)
                            <option
                                value="{{ $client->id }}" {{ (isset($rent) && $rent->client_id == $client->id) ? 'selected' : '' }}>
                                {{ $client->name }}
                            </option>
                        @endforeach
                    </select>
                </div>

                <div class="mb-4">
                    <label for="inventory_id" class="block text-gray-700 text-sm font-bold mb-2">Инвентарь:</label>
                    <select name="inventory_id" id="inventory_id" required
                            class="shadow border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline">
                            <option value="">
                                *Не выбрано*
                            </option>
                    </select>
                </div>

                <div class="mb-4">
                    <label for="tariff_type" class="block text-gray-700 text-sm font-bold mb-2">Тип аренды:</label>
                    <select name="tariff_type" id="tariff_type" required
                            class="shadow border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline">
                        <option value="daily">Посуточно</option>
                        <option value="weekly">Понедельно</option>
                    </select>
                </div>

                <div class="mb-4">
                    <label for="duration" class="block text-gray-700 text-sm font-bold mb-2">Количество
                        дней/недель:</label>
                    <input type="number" name="duration" id="duration" required
                           class="shadow border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                           min="1" value="1">
                </div>

                <div class="mb-4">
                    <label for="start_date" class="block text-gray-700 text-sm font-bold mb-2">Дата начала:</label>
                    <input type="date" name="start_date" id="start_date" value="{{ $rent->start_date ?? '' }}" required
                           class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline">
                </div>

                <div class="mb-4">
                    <label for="end_date" class="block text-gray-700 text-sm font-bold mb-2">Дата окончания:</label>
                    <input type="date" name="end_date" id="end_date" value="{{ $rent->end_date ?? '' }}"
                           readonly
                           class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline">
                </div>

                <div class="mb-4">
                    <label for="total_cost" class="block text-gray-700 text-sm font-bold mb-2">Общая стоимость:</label>
                    <input type="text" name="total_cost" id="total_cost" value="{{ $rent->total_cost ?? '' }}" required
                           readonly
                           class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline">
                </div>

                <div class="mb-4">
                    <label for="status" class="block text-gray-700 text-sm font-bold mb-2">Статус:</label>
                    <select name="status" id="status" required
                            class="shadow border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline">
                        <option value="active" {{ (isset($rent) && $rent->status == 'active') ? 'selected' : '' }}>
                            Активна
                        </option>
                        <option value="closed" {{ (isset($rent) && $rent->status == 'closed') ? 'selected' : '' }}>
                            Закрыта
                        </option>
                    </select>
                </div>

                <div class="flex items-center justify-between">
                    <button type="submit"
                            class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline">
                        Сохранить
                    </button>
                </div>
                @push('scripts')
                    <script>
                        document.addEventListener('DOMContentLoaded', function() {
                            axios.get('{{ route("inventories.available") }}')
                                .then(function(response) {
                                    const data = response.data;
                                    const select = document.getElementById('inventory_id');
                                    select.disabled;

                                    data.forEach(function(inventory) {
                                        const option = document.createElement('option');
                                        option.value = inventory.id;
                                        option.textContent = inventory.name;
                                        option.dataset.pricePerDay = inventory.price_per_day;
                                        option.dataset.pricePerWeek = inventory.price_per_week;
                                        select.appendChild(option);
                                    });
                                    select.enabled;
                                })
                                .catch(function(error) {
                                    console.log(error);
                                });
                        });
                    </script>

                @endpush
            </form>
