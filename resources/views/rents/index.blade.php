@extends('layouts.app')

@section('content')
    <div class="container mx-auto px-4">
        <h1 class="text-2xl font-bold mb-4">Аренды</h1>
        <a href="{{ route('analytic.rents') }}" class="bg-green-500 hover:bg-green-700 text-white font-bold py-2 px-4 rounded">Аналитика</a>
        <a href="{{ route('rents.create') }}" class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded">Добавить аренду</a>
        <table class="table-auto w-full mt-4">
            <thead>
            <tr class="bg-gray-200">
                <th class="px-4 py-2">Сотрудник</th>
                <th class="px-4 py-2">Клиент</th>
                <th class="px-4 py-2">Инвентарь</th>
                <th class="px-4 py-2">Дата начала</th>
                <th class="px-4 py-2">Дата окончания</th>
                <th class="px-4 py-2">Стоимость</th>
                <th class="px-4 py-2">Статус</th>
                <th class="px-4 py-2">Действия</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($rents as $rent)
                <tr>
                    <td class="border px-4 py-2">{{ $rent->employee->name }}</td>
                    <td class="border px-4 py-2">{{ $rent->client->name }}</td>
                    <td class="border px-4 py-2">{{ $rent->inventory->name }}</td>
                    <td class="border px-4 py-2">{{ $rent->start_date }}</td>
                    <td class="border px-4 py-2">{{ $rent->end_date ?? 'Не указано' }}</td>
                    <td class="border px-4 py-2">{{ $rent->total_cost }}</td>
                    <td class="border px-4 py-2">
                        {{ $rent->status == 'active' ? 'Активна' : 'Закрыта' }}
                    </td>
                    <td class="border px-4 py-2 flex justify-around">
                        @if ($rent->status === 'active')
                            <form action="{{ route('rents.update', $rent->id) }}" method="POST" onsubmit="return confirm('Вы уверены, что хотите ЗАКРЫТЬ эту аренду?');">
                                @csrf
                                @method('PUT')
                                <button type="submit"
                                        class="bg-green-500 hover:bg-green-700 text-white font-bold py-1 px-3 rounded">
                                    Закрыть
                                </button>
                            </form>
                        @endif
                        <form action="{{ route('rents.destroy', $rent->id) }}" method="POST" onsubmit="return confirm('Вы уверены, что хотите УДАЛИТЬ эту аренду?');">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="bg-red-500 hover:bg-red-700 text-white font-bold py-1 px-3 rounded">Удалить</button>
                        </form>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection
