@extends('layouts.app')

@section('content')
    <div class="container mx-auto px-4">
        <h1 class="text-2xl font-bold mb-4">Добавить аренду</h1>
        @include('rents.form', ['action' => route('rents.store')])
    </div>
    @push('scripts')
        <script>
            document.addEventListener('DOMContentLoaded', function () {
                const calculateEndDateAndTotalCost = () => {
                    const tariffType = document.getElementById('tariff_type').value;
                    const duration = parseInt(document.getElementById('duration').value, 10) || 0;
                    const startDate = document.getElementById('start_date').value;
                    const inventoryId = document.getElementById('inventory_id').value;
                    const inventoryOption = document.querySelector(`#inventory_id option[value="${inventoryId}"]`);
                    let endDate = new Date(startDate);
                    let totalCost = 0;

                    let pricePerDay = 0;
                    let pricePerWeek = 0;

                    if (inventoryOption) {
                        pricePerDay = parseFloat(inventoryOption.dataset.pricePerDay) || 0;
                        pricePerWeek = parseFloat(inventoryOption.dataset.pricePerWeek) || 0;
                    }

                    if (tariffType === 'daily') {
                        endDate.setDate(endDate.getDate() + duration);
                        totalCost = pricePerDay * duration;
                    } else if (tariffType === 'weekly') {
                        endDate.setDate(endDate.getDate() + (duration * 7));
                        totalCost = pricePerWeek * duration;
                    }

                    document.getElementById('end_date').valueAsDate = endDate;
                    document.getElementById('total_cost').value = totalCost.toFixed(2);
                };

                const elements = ['tariff_type', 'duration', 'start_date', 'inventory_id'];

                elements.forEach((element) => {
                    document.getElementById(element).addEventListener('change', calculateEndDateAndTotalCost);
                    document.getElementById(element).addEventListener('input', calculateEndDateAndTotalCost);
                });

                calculateEndDateAndTotalCost();
            });

        </script>
    @endpush
@endsection
