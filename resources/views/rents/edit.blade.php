@extends('layouts.app')

@section('content')
    <div class="container mx-auto px-4">
        <h1 class="text-2xl font-bold mb-4">Редактировать аренду</h1>
        @include('rents.form', ['action' => route('rents.update', $rent->id), 'rent' => $rent])
    </div>
@endsection
