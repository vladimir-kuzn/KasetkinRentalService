<form action="{{ $action }}" method="POST" class="w-full max-w-lg">
    @csrf
    @if(isset($inventory)) @method('PUT') @endif

    <div class="mb-4">
        <label for="name" class="block text-gray-700 text-sm font-bold mb-2">Наименование:</label>
        <input type="text" name="name" id="name" value="{{ $inventory->name ?? '' }}" required class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline">
    </div>

    <div class="mb-4">
        <label for="price_per_day" class="block text-gray-700 text-sm font-bold mb-2">Цена за сутки:</label>
        <input type="text" name="price_per_day" id="price_per_day" value="{{ $inventory->price_per_day ?? '' }}" required class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline">
    </div>

    <div class="mb-4">
        <label for="price_per_week" class="block text-gray-700 text-sm font-bold mb-2">Цена за неделю:</label>
        <input type="text" name="price_per_week" id="price_per_week" value="{{ $inventory->price_per_week ?? '' }}" required class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline">
    </div>

    <div class="mb-4">
        <label for="status" class="block text-gray-700 text-sm font-bold mb-2">Статус:</label>
        <select disabled name="status" id="status" class="shadow border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline">
            <option value="free" {{ (isset($inventory) && $inventory->status == 'free') ? 'selected' : '' }}>Свободен</option>
            <option value="in_rent" {{ (isset($inventory) && $inventory->status == 'in_rent') ? 'selected' : '' }}>В аренде</option>
        </select>
    </div>

    <div class="flex items-center justify-between">
        <button type="submit" class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline">Сохранить</button>
    </div>
</form>
