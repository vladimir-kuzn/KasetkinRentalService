@extends('layouts.app')

@section('content')
    <div class="container mx-auto px-4">
        <h1 class="text-2xl font-bold mb-4">Инвентарь</h1>
        <a href="{{ route('inventories.create') }}" class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded">Добавить предмет</a>
        <table class="table-auto w-full mt-4">
            <thead>
            <tr class="bg-gray-200">
                <th class="px-4 py-2">Наименование</th>
                <th class="px-4 py-2">Цена за сутки</th>
                <th class="px-4 py-2">Цена за неделю</th>
                <th class="px-4 py-2">Статус</th>
                <th class="px-4 py-2">Действия</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($inventories as $inventory)
                <tr>
                    <td class="border px-4 py-2">{{ $inventory->name }}</td>
                    <td class="border px-4 py-2">{{ $inventory->price_per_day }}</td>
                    <td class="border px-4 py-2">{{ $inventory->price_per_week }}</td>
                    <td class="border px-4 py-2">{{ $inventory->status == 'in_rent' ? 'В аренде' : 'Свободен' }}</td>
                    <td class="border px-4 py-2 flex justify-around">
                        <a href="{{ route('inventories.edit', $inventory->id) }}" class="bg-yellow-500 hover:bg-yellow-700 text-white font-bold py-1 px-3 rounded">Изменить</a>
                        <form action="{{ route('inventories.destroy', $inventory->id) }}" method="POST" onsubmit="return confirm('Вы уверены, что хотите удалить этот предмет?');">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="bg-red-500 hover:bg-red-700 text-white font-bold py-1 px-3 rounded">Удалить</button>
                        </form>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection
