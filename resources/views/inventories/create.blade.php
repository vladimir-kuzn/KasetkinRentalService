@extends('layouts.app')

@section('content')
    <div class="container mx-auto px-4">
        <h1 class="text-2xl font-bold mb-4">Добавить предмет</h1>
        @include('inventories.form', ['action' => route('inventories.store')])
    </div>
@endsection
