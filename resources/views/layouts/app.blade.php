<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    <!-- Fonts -->
    <link rel="preconnect" href="https://fonts.bunny.net">
    <link href="https://fonts.bunny.net/css?family=figtree:400,600&display=swap" rel="stylesheet" />

    <!-- Scripts -->
    <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>

    <!-- Styles -->
    <script src="https://cdn.tailwindcss.com"></script>
</head>
<body class="antialiased min-h-screen">

    <header class="bg-blue-900 text-white py-4">
        <div class="container mx-auto flex justify-between items-center">
            <h1 class="text-xl">Магазин "Пират Касеткин"</h1>
            <nav>
                <ul class="flex space-x-4">
                    <li><a href="{{ route('employees.index') }}" class="hover:text-gray-200">Сотрудники</a></li>
                    <li><a href="{{ route('clients.index') }}" class="hover:text-gray-200">Клиенты</a></li>
                    <li><a href="{{ route('inventories.index') }}" class="hover:text-gray-200">Инвентарь</a></li>
                    <li><a href="{{ route('rents.index') }}" class="hover:text-gray-200">Аренды</a></li>
                </ul>
            </nav>
        </div>
    </header>

    <main class="py-8">
        <div class="container mx-auto">
            @yield('content')
        </div>
    </main>

    @stack('scripts')
</body>
</html>
