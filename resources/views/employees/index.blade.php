@extends('layouts.app')

@section('content')
    <div class="container mx-auto px-4">
        <h1 class="text-2xl font-bold mb-4">Сотрудники</h1>
        <a href="{{ route('analytic.employees') }}" class="bg-green-500 hover:bg-green-700 text-white font-bold py-2 px-4 rounded">Аналитика</a>
        <a href="{{ route('employees.create') }}" class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded">Добавить сотрудника</a>
        <table class="table-auto w-full mt-4">
            <thead>
            <tr class="bg-gray-200">
                <th class="px-4 py-2">Имя</th>
                <th class="px-4 py-2">Email</th>
                <th class="px-4 py-2">Должность</th>
                <th class="px-4 py-2">Действия</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($employees as $employee)
                <tr>
                    <td class="border px-4 py-2">{{ $employee->name }}</td>
                    <td class="border px-4 py-2">{{ $employee->email }}</td>
                    <td class="border px-4 py-2">{{ $employee->position }}</td>
                    <td class="border px-4 py-2 flex justify-around">
                        <a href="{{ route('employees.edit', $employee->id) }}" class="bg-yellow-500 hover:bg-yellow-700 text-white font-bold py-1 px-3 rounded">Изменить</a>
                        <form action="{{ route('employees.destroy', $employee->id) }}" method="POST" onsubmit="return confirm('Вы уверены, что хотите удалить этого сотрудника?');">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="bg-red-500 hover:bg-red-700 text-white font-bold py-1 px-3 rounded">Удалить</button>
                        </form>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection
