<form action="{{ $action }}" method="POST" class="w-full max-w-lg">
    @csrf
    @if(isset($employee)) @method('PUT') @endif

    <div class="mb-4">
        <label for="name" class="block text-gray-700 text-sm font-bold mb-2">Имя:</label>
        <input type="text" name="name" id="name" value="{{ $employee->name ?? '' }}" required class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline">
    </div>

    <div class="mb-4">
        <label for="email" class="block text-gray-700 text-sm font-bold mb-2">Email:</label>
        <input type="email" name="email" id="email" value="{{ $employee->email ?? '' }}" required class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline">
    </div>

    <div class="mb-4">
        <label for="position" class="block text-gray-700 text-sm font-bold mb-2">Должность:</label>
        <input type="text" name="position" id="position" value="{{ $employee->position ?? '' }}" required class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline">
    </div>

    <div class="flex items-center justify-between">
        <button type="submit" class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline">Сохранить</button>
    </div>
</form>
