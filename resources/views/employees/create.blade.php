@extends('layouts.app')

@section('content')
    <div class="container mx-auto px-4">
        <h1 class="text-2xl font-bold mb-4">Добавить сотрудника</h1>
        @include('employees.form', ['action' => route('employees.store')])
    </div>
@endsection
