@extends('layouts.app')

@section('content')
    <div class="container mx-auto px-4">
        <h1 class="text-2xl font-bold mb-4">График доходов по каждому сотруднику</h1>

        <form action="{{ route('analytic.employees') }}" method="GET" class="mb-4">
            <input type="text" name="search" placeholder="Поиск по ФИО" class="mr-2">
            <input type="date" name="start_date" class="mr-2" placeholder="Начальная дата">
            <input type="date" name="end_date" class="mr-2" placeholder="Конечная дата">
            <button type="submit" class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded">Поиск</button>
        </form>

        @if($employees->isNotEmpty())
            <table class="table-auto w-full mt-4">
                <thead>
                <tr class="bg-gray-200">
                    <th class="px-4 py-2">Имя</th>
                    <th class="px-4 py-2">Email</th>
                    <th class="px-4 py-2">Должность</th>
                    <th class="px-4 py-2">Доход за период</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($employees as $employee)
                    <tr>
                        <td class="border px-4 py-2">{{ $employee->name }}</td>
                        <td class="border px-4 py-2">{{ $employee->email }}</td>
                        <td class="border px-4 py-2">{{ $employee->position }}</td>
                        <td class="border px-4 py-2">
                            @if($employee->rents->isNotEmpty())
                                {{ $employee->rents->first()->total_income }}
                            @else
                                0
                            @endif
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        @else
            <p>Сотрудники не найдены.</p>
        @endif
    </div>
@endsection
