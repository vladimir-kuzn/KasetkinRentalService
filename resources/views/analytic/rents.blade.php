@extends('layouts.app')

@section('content')
    <div class="container mx-auto px-4">
        <h1 class="text-2xl font-bold mb-4">График доходов от аренды по дням за все время</h1>

        <div>
            <canvas id="myChart"></canvas>
        </div>
    </div>
    @push('scripts')
        <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
        <script>
            document.addEventListener('DOMContentLoaded', function () {
                const labels = @json($labels);
                const dataPoints = @json($data).map(point => point === null ? NaN : point);

                const ctx = document.getElementById('myChart').getContext('2d');
                const myChart = new Chart(ctx, {
                    type: 'line',
                    data: {
                        labels: labels,
                        datasets: [{
                            label: 'Статистика аренд по дням',
                            data: dataPoints,
                            borderColor: 'rgb(75, 192, 192)',
                            backgroundColor: 'rgba(75, 192, 192, 0.5)',
                            segment: {
                                borderDash: ctx => skipped(ctx, [6, 6]),
                            },
                            spanGaps: true,
                        }]
                    },
                    options: {
                        fill: false,
                        interaction: {
                            intersect: false
                        },
                        radius: 0,
                        scales: {
                            y: {
                                beginAtZero: true
                            }
                        }
                    }
                });

                function skipped(ctx, value = [6, 6]) {
                    const {p0, p1} = ctx;
                    // Проверяем, есть ли пропущенные данные
                    return (p0.skip || p1.skip) ? value : undefined;
                }
            });
        </script>
    @endpush
@endsection
