<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Rent extends Model
{
    use HasFactory;

    protected $fillable = [
        'employee_id',
        'client_id',
        'inventory_id',
        'start_date',
        'end_date',
        'total_cost',
        'status'
    ];

    public function employee() :BelongsTo
    {
        return $this->belongsTo(Employee::class);
    }

    public function client() :BelongsTo
    {
        return $this->belongsTo(Client::class);
    }

    public function inventory() :BelongsTo
    {
        return $this->belongsTo(Inventory::class);
    }
}
