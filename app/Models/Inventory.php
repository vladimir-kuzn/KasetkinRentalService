<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Inventory extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'price_per_day',
        'price_per_week'
    ];

    public function rents() :HasMany
    {
        return $this->hasMany(Rent::class, 'inventory_id');
    }

    public function getStatusAttribute()
    {
        $activeRent = $this->rents()->where('status', 'active')
            ->first();

        return $activeRent ? 'in_rent' : 'free';
    }

}
