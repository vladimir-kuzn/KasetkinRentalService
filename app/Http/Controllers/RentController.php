<?php

namespace App\Http\Controllers;

use App\Http\Requests\RentRequest;
use App\Models\Client;
use App\Models\Employee;
use App\Models\Inventory;
use App\Models\Rent;
use Illuminate\Http\Request;

class RentController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $rents = Rent::with(['employee', 'client', 'inventory'])->get();
        return view('rents.index', compact('rents'));
    }

    public function create()
    {
        return view('rents.create', [
            'employees' => Employee::all(),
            'clients' => Client::all(),
            'inventories' => $this->getAvailableInventories(),
        ]);
    }

    public function store(RentRequest $request)
    {
        Rent::create($request->all());

        return redirect()->route('rents.index')->with('success', 'Аренда успешно добавлена.');
    }

    public function show(Rent $rent)
    {
        abort(501, 'Функционал просмотра ещё не разработан.');
    }


    public function edit(Rent $rent)
    {
        return view('rents.edit', [
            'rent' => $rent,
            'employees' => Employee::all(),
            'clients' => Client::all(),
            'inventories' => $this->getAvailableInventories($rent),
        ]);
    }


    public function update(/*RentRequest $request,*/ Rent $rent)
    {
//        $rent->update($request->all());
        $rent->status = "closed";
        $rent->save();

        return redirect()->route('rents.index')->with('success', 'Информация об аренде обновлена.');
    }


    public function destroy(Rent $rent)
    {
        $rent->delete();

        return back()->with('success', 'Аренда удалена.');
    }

    public function getAvailableInventoriesAjax(Request $request)
    {
        $rentId = $request->input('rent_id');
        $rent = $rentId ? Rent::find($rentId) : null;

        $inventories = $this->getAvailableInventories($rent);

        return response()->json($inventories);
    }

    private function getAvailableInventories(Rent $rent = null)
    {
        $query = Inventory::query();

        if ($rent) {
            $query->whereDoesntHave('rents', function ($query) use ($rent) {
                $query->where('status', 'active')->where('id', '!=', $rent->id);
            })->orWhere('id', $rent->inventory_id);
        } else {
            $query->doesntHave('rents', 'and', function ($query) {
                $query->where('status', 'active');
            });
        }

        return $query->get();
    }
}
