<?php

namespace App\Http\Controllers;

use App\Models\Employee;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class AnalyticController extends Controller
{
    public function rents()
    {
        $rents = DB::table('rents')
            ->select(DB::raw('DATE(start_date) as day'), DB::raw('SUM(total_cost) as amount'))
            ->groupBy(DB::raw('DATE(start_date)'))
            ->orderBy(DB::raw('DATE(start_date)'))
            ->get();

        $startDate = $rents->first()->day;
        $endDate = $rents->last()->day;

        $start = Carbon::createFromFormat('Y-m-d', $startDate);
        $end = Carbon::createFromFormat('Y-m-d', $endDate);

        $labels = [];
        $data = [];
        // Преобразование коллекции в массив с датой в качестве ключа
        $rentsArray = $rents->pluck('amount', 'day')->toArray();

        for ($date = $start; $date->lte($end); $date->addDay()) {
            $formattedDate = $date->format('Y-m-d');
            $labels[] = $formattedDate;
            $data[] = array_key_exists($formattedDate, $rentsArray) ? (float)$rentsArray[$formattedDate] : 'NaN';
        }

        return view('analytic.rents', ['labels' => $labels, 'data' => $data]);
    }

    public function employees(Request $request)
    {
        $query = Employee::query();

        if ($search = $request->input('search')) {
            $query->where('name', 'like', "%{$search}%");
        }

        $startDate = $request->input('start_date');
        $endDate = $request->input('end_date');

        // Добавляем агрегацию доходов сотрудников по сделкам в указанный период
        $employees = $query->with(['rents' => function ($query) use ($startDate, $endDate) {
            if ($startDate && $endDate) {
                $query->whereBetween('start_date', [$startDate, $endDate]);
            }
            // Группируем по employee_id и агрегируем сумму доходов
            $query->selectRaw('employee_id, SUM(total_cost) as total_income')
                ->groupBy('employee_id');
        }])->get();

        return view('analytic.employees', compact('employees'));
    }
}
