<?php

namespace App\Http\Controllers;

use App\Models\Inventory;
use Illuminate\Http\Request;

class InventoryController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $inventories = Inventory::all();
        return view('inventories.index', compact('inventories'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('inventories.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|string|max:255',
            'price_per_day' => 'required|numeric|min:0|max:999999.99',
            'price_per_week' => 'required|numeric|min:0|max:999999.99',
        ]);

        Inventory::create($request->all());

        return redirect()->route('inventories.index')->with('success', 'Инвентарь успешно добавлен.');
    }

    /**
     * Display the specified resource.
     */
    public function show(Inventory $inventory)
    {
        abort(501, 'Функционал просмотра ещё не разработан.');
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Inventory $inventory)
    {
        return view('inventories.edit', compact('inventory'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Inventory $inventory)
    {
        $request->validate([
            'name' => 'required|string|max:255|unique:inventories,name,' . $inventory->id,
            'price_per_day' => 'required|numeric|min:0|max:999999.99',
            'price_per_week' => 'required|numeric|min:0|max:999999.99',
        ]);

        $inventory->update($request->all());

        return redirect()->route('inventories.index')->with('success', 'Информация о инвентаре обновлена.');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Inventory $inventory)
    {
        $inventory->delete();

        return redirect()->route('inventories.index')->with('success', 'Инвентарь удален.');
    }
}
