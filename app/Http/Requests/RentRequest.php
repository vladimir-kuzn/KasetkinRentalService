<?php

namespace App\Http\Requests;

use App\Rules\InventoryAvailable;
use Illuminate\Foundation\Http\FormRequest;

class RentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'employee_id' => 'required|exists:employees,id',
            'client_id' => 'required|exists:clients,id',
            'inventory_id' => [
                'required',
                'exists:inventories,id',
                new InventoryAvailable($this->route('rent')?->id)
            ],
            'start_date' => 'required|date',
            'end_date' => 'nullable|date|after_or_equal:start_date',
            'total_cost' => 'required|numeric|min:0|max:999999.99',
            'status' => 'required|in:active,closed',
        ];
    }
}
