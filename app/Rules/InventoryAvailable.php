<?php

namespace App\Rules;

use Closure;
use Illuminate\Contracts\Validation\ValidationRule;
use App\Models\Rent;

class InventoryAvailable implements ValidationRule
{
    protected $rentId;

    // Добавляем конструктор для возможности исключения текущей аренды из проверки
    public function __construct($rentId = null)
    {
        $this->rentId = $rentId;
    }

    /**
     * Run the validation rule.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @param  Closure(string): \Illuminate\Translation\PotentiallyTranslatedString  $fail
     */
    public function validate(string $attribute, mixed $value, Closure $fail): void
    {
        $query = Rent::where('inventory_id', $value)
            ->where('status', 'active');

        // Если у нас есть ID аренды, исключаем ее из проверки
        if ($this->rentId) {
            $query->where('id', '!=', $this->rentId);
        }

        if ($query->exists()) {
            $fail('Инвентарь уже находится в аренде.');
        }
    }
}

