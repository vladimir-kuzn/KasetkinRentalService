<?php

use App\Http\Controllers\AnalyticController;
use App\Http\Controllers\ClientController;
use App\Http\Controllers\EmployeeController;
use App\Http\Controllers\InventoryController;
use App\Http\Controllers\RentController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('inventories/available', [RentController::class, 'getAvailableInventoriesAjax'])->name('inventories.available');

Route::resource('employees', EmployeeController::class);
Route::resource('clients', ClientController::class);
Route::resource('inventories', InventoryController::class);
Route::resource('rents', RentController::class);

Route::get('analytic/rents', [AnalyticController::class, 'rents'])->name('analytic.rents');
Route::get('analytic/employees', [AnalyticController::class, 'employees'])->name('analytic.employees');

