# Тестовое задание PHP-разработчика.
## Описание задания
Разработать мини-сайт магазина «Пират Касеткин». Директор магазина - очень бдительный человек, и
он любит за всем следить самостоятельно. Ему очень необходимо, чтобы аренды формировались корректно,
чтобы не потерять «сокровища». Ему важно: следить за доходами от аренд, какой сотрудник приносит
больший доход.
## Базовые сущности
В ходе разработки могут быть созданы дополнительные таблицы, для связи сущностей, если того будет
требовать ваше решение поставленной задачи.
- **Сотрудник магазина (Employee).** Пользователь системы, отвечающий за выдачу инвентаря в аренду
клиентам.
- **Клиент(Client).** Пользователь системы, который берет инвентарь в аренду и оплачивает заказ.
- **Инвентарь(Inventory).** Предмет, который сдается магазином в прокат, имеет тарифную сетку: сутки или
неделю – с различной ценой. Инвентарь может находится в одном из двух состояний: «в аренде»,
«свободен». Инвентарь считается свободным, если он не находится на текущий момент в аренде, или аренда
была закрыта.
- **Аренда(Rent).** Формальный договор, между клиентом и сотрудником, о том, что сотрудник
предоставляет, выбранный клиентом инвентарь, в личное пользование на оговоренный срок, на указанную
сумму денег.
## Требования к сайту и данным
- Разработать сайт на web-фраймворке Laravel (не ниже 5.6).
- База данных MySql, PostgreSql(Желательно).
- Количество записей сотрудников, клиентов не менее 5. Можно заполнить единоразово в БД, можно
сделать интерфейс для добавления.
- Для инвентаря должен быть реализован интерфейс для добавления, удаления и редактирования
данных.
- Необходимо реализовать интерфейс для добавления новых аренд или закрытия имеющихся (когда
аренда закрывается – инвентарь, взятый в прокат, изменяет статус на «свободен»).
- Необходимо учесть, что инвентарь, находящийся в аренде, не может быть сдан повторно в аренду, в то
же время.
- Организация системы аутентификации директора не обязательно, но не хочется вас лишать
проявления творческого креатива.
- При оформлении аренд загрузка свободного инвентаря должна производиться ajax-запросом к сайту.
- Необходимо вывести график доходов от аренды по дням за все время.
- Необходимо вывести таблицу с доходами по каждому сотруднику с возможностью поиска по ФИО.
Дополнительно: реализовать фильтр по выдаче доходов сотрудников на указанный интервал времени
(месяц, неделя).
## Результаты
- Результат представить ссылкой на репозиторий. Желательно, в репозиторий залить сначала пустой
каркас сайта, а затем с внесёнными изменениями, чтобы можно было проследить ход работы.
- Структуру базы данных и данные для работы можно отправить архивом.
